/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef COGNITION_H
#define	COGNITION_H

#include <xc.h> // include processor files - each processor file is guarded.  

    /**
     * 2 main functions
     */
    
    /**
     * Function to call on bird arrival 
     * @param raw_rfid - scanned bird's rfid
     */
    void bird_arrived(uint8_t * raw_rfid);
    
    /**
     * Function to call on message arrival
     * @param rfid - rfid in message
     */
    void update_bird(uint8_t * rfid);
    
    /*********** END MAIN FUNCTIONS **************/
    
    /**
     * Initialize cognition program with given size
     * @param size
     */
    void cognition_init(uint8_t size);

    /***
     * Set new cognition list from given array
     * @param new_cognition - array containing new cognition
     * @param size - size of the list
     */
    void cognition_set(uint8_t * new_cognition, uint8_t size);

    /***
     * Get element from cognition list
     * @param number - index in cognition list
     * @return value in behaviour list or -1
     */
    uint8_t cognition_get(uint8_t number);
    
    
    /***
     * DICT dictionary
     */
    
    /**
     * Initialize dictionary size to 0
     */
    void DICT_init();
    
    /**
     * Add bird to dictionary
     * @param rfid - bird's rfid
     * @return 0 successful, -1 if out of space
     */
    uint8_t DICT_add_bird(const uint8_t * rfid);
    
    /**
     * Get bird's position in cognition program. 
     * If bird doe not exist, it's record is created. 
     * @param rfid - bird's rfid
     * @return position if bird exists, -1 if not
     */
    uint8_t DICT_get_bird_cognition_index(const uint8_t * rfid);
    
    /**
     * Get bird's target in current cognition program 
     * If bird doe not exist, it's record is created. 
     * @param rfid - bird's rfid
     * @return bird's target, -1 if not
     */
    uint8_t DICT_get_bird_current_cognition(const uint8_t * rfid);
    
    /**
     * Updates bird's position in cognition program in the dictionary
     * @param rfid - bird's rfid
     */
    void DICT_update_bird(const uint8_t * rfid);
    
    /**
     * Function that is executed on bird arrival
     * @param raw_rfid - bird's rfid [10 char]
     */
    void bird_arrived(uint8_t * raw_rfid);
    
    /**
     * Function that is executed on bird arrival
     * @param raw_rfid - bird's rfid [5 char]
     */
    void update_bird(uint8_t * rfid);
    

#endif	/* COGNITION_H */

