/*
 * File:   RFID.c
 * Author: dehuit
 *
 * Created on November 7, 2019, 6:32 PM
 */


#include "xc.h"
#include "RFID.h"

/***
 * Compress RFID in 2 times
 * @param rfid_raw - RFID as it was read (10 uint8_tacters)
 * @param rfid_compressed - RFID as 5 nu
 */
void RFID_compress(const uint8_t * rfid_raw, uint8_t * rfid_compressed) {
    int i;
    for (i = 0; i < RFID_SIZE; i++) {
        int j = i*2; // 0 2 4 6 9
        // Explication : 58 is the next uint8_tacter to '9', 48 is '0' in ASCII, 55 is a 'A'(65) -10 in ascii to get HEX value dirrectly
        rfid_compressed[i] = ((rfid_raw[j] <= '9') ? rfid_raw[j] - '0': rfid_raw[j] - 55) << 4;
        rfid_compressed[i] += (rfid_raw[j+1] <= '9') ? rfid_raw[j+1] - '0': rfid_raw[j+1] - 55;
    }
}

/**
 * Compare 2 RFID
 * @param rfid_a - first RFID
 * @param rfid_b - second RFID
 * @return - 1 (true) if they are the same, 0 (false) if not
 */
uint8_t RFID_cmp(const uint8_t * rfid_a, const uint8_t * rfid_b) {
   uint8_t equal = 1;
   int i;
   for (i = 0; equal && i < RFID_SIZE; i++)
       equal = equal && (rfid_a[i] == rfid_b[i]);
   return equal;
}